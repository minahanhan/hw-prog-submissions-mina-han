package com.example.mkh.prog03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.lang.InterruptedException;
import android.widget.TextView;

public class InstActivity extends AppCompatActivity {

    private int lat;
    private int lon;
    private List<String> imageURLs;
    private String imgUrl;
    String firstString;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inst);
        Bundle extras = getIntent().getExtras();
        String value = "0,0";
        if (extras != null) {
            value = extras.getString("location");
        }
        String [] values = value.split(",");
        float latString = Float.parseFloat(values[0]);
        float lonString = Float.parseFloat(values[1]);
        lat = Math.round(latString);
        lon = Math.round(lonString);
        try{
            thread.start();
            thread.join();
        } catch(InterruptedException e){
            e.printStackTrace();
            return;
        }
        TextView errorLog = (TextView) findViewById(R.id.error);
        //try {
            //String imageUrl = imageURLs.get(0);
            ImageView i = (ImageView)findViewById(R.id.imageView1);
            //imgUrl = "https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e15/10852788_1532572413681710_839244255_n.jpg";
//            Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(imgUrl).getContent());
            i.setImageBitmap(bitmap);
//        } catch (MalformedURLException e) {
//            errorLog.setText("didn't get bitmap. firstString is " + firstString);
//            e.printStackTrace();
//        } catch (IOException e) {
//            errorLog.setText("IOexception");
//            e.printStackTrace();
//        }
    }

    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            TextView errorLog = (TextView) findViewById(R.id.error);
            lat = 10;
            lon = 10;
            String InstagramRequest = "https://api.instagram.com/v1/media/search?lat=" + Integer.toString(lat) + "&lng=" + Integer.toString(lon) + "&client_id=7d45f1c58a79489cbc47c92c402d0e93&distance=200";
            String responseText = "response text";
            JSONObject mainResponseObject;
            imageURLs = new ArrayList<String>();
            String imgUrl = "img";
            try {
                responseText = getResponseText(InstagramRequest);

            } catch (IOException e){
                errorLog.setText("didn't get responseText");
                e.printStackTrace();
                return;
            }
            try {
                mainResponseObject = new JSONObject(responseText);
            } catch (JSONException e) {
                errorLog.setText("No pictures to display");
                e.printStackTrace();
                return;
            }
            if (mainResponseObject.length() == 0){
                errorLog.setText("didn't get response");
                return;
            } else {
                    String jsondata = mainResponseObject.toString();
                    int start = jsondata.indexOf("standard_resolution");
//                    String sub = jsondata.substring(start,start+20);
//                    int end = sub.indexOf(",");
//                    sub = sub.substring(0,end);
//                    start = sub.indexOf("https:");
//                    end = sub.indexOf(".jpg");
//                    imgUrl = sub.substring(start,end+4);
//                    imgUrl = imgUrl.replace("\\","");
//                    imgUrl = imgUrl.replace(" ","");
                imgUrl = "https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e35/11906239_1012692635447995_30143647_n.jpg";
                    firstString = imgUrl;
                    try{
                        bitmap = BitmapFactory.decodeStream((InputStream) new URL(imgUrl).getContent());
                    } catch (MalformedURLException e) {
                        errorLog.setText("didn't get bitmap. firstString is " + firstString);
                        e.printStackTrace();
                    } catch (IOException e) {
                        errorLog.setText("IOexception");
                        e.printStackTrace();
                    }
//                    for (int i = 0; i < array.length(); i++) {
//                        JSONObject o = array.getJSONObject(8);
//                        imageURLs.add(o.getJSONObject("standard resolution").getString("url"));
//                    }
                    //imgUrl = images.getJSONObject("standard_resolution").getString("url");
//                    JSONObject imgs = (JSONObject) data.get("images");
//                    JSONObject std = (JSONObject) imgs.get("standard_resolution");
//                    imgUrl = (String) std.get("url");
                //} catch (JSONException e) {
                    //errorLog.setText("didn't get URL");
                    //e.printStackTrace();
                    //return;
                //}

//                try {
//                    //String imageUrl = imageURLs.get(0);
//                    ImageView i = (ImageView)findViewById(R.id.imageView1);
//                    Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(imgUrl).getContent());
//                    i.setImageBitmap(bitmap);
//                } catch (MalformedURLException e) {
//                    errorLog.setText("didn't get bitmap");
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    errorLog.setText("IOexception");
//                    e.printStackTrace();
//                }
            }
        };
    });

    private String getResponseText(String stringUrl) throws IOException {
        StringBuilder response  = new StringBuilder();

        URL url = new URL(stringUrl);
        HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
        if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
        {
            BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
            String strLine = null;
            while ((strLine = input.readLine()) != null)
            {
                response.append(strLine);
            }
            input.close();
        }
        return response.toString();
    }

}
