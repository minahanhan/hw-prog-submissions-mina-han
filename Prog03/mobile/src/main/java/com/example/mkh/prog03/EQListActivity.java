package com.example.mkh.prog03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.content.Intent;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import java.util.HashMap;

public class EQListActivity extends AppCompatActivity {

    private ArrayList<String> eqStrings;
    private HashMap<String,String> coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eqlist);
        try{
            thread.start();
            thread.join();
        } catch(InterruptedException e){
            e.printStackTrace();
            return;
        }

        final ListView lv = (ListView) findViewById(R.id.listview);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
               eqStrings );

        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String content = (String)(lv.getItemAtPosition(position));
                String location = coordinates.get(content);
                Intent i = new Intent(EQListActivity.this, MapsActivity.class);
                i.putExtra("location", location);
                startActivity(i);
            }
        });
    }

    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String strDate = sdf.format(c.getTime());
            strDate = strDate.split(" ")[0];
            strDate.concat("T00:00:00");
            String EQResponse = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime="+strDate;
            String responseText;
            try {
                responseText = getResponseText(EQResponse);

            } catch (IOException e){
                e.printStackTrace();
                return;
            }
            String [] raw_json = responseText.split("Feature");
            int start;
            int end;
            String title;
            String coordinatesBreak;
            String [] coordinatesPieces;
            eqStrings = new ArrayList<String>();
            coordinates = new HashMap<String,String>();
            String firstC;
            String secondC;
            for (int i = 2; i<raw_json.length;i++){
                start = raw_json[i].indexOf("title");
                end = raw_json[i].indexOf("}");
                title = raw_json[i].substring(start + 8, end - 1);
                start = raw_json[i].indexOf("coordinates");
                coordinatesBreak = raw_json[i].substring(start + 14, raw_json[i].length());
                end = coordinatesBreak.indexOf("]");
                coordinatesBreak = coordinatesBreak.substring(0,end);
                eqStrings.add(title);
                //eqStrings.add(coordinatesBreak);
                coordinatesPieces = coordinatesBreak.split(",");
                firstC = coordinatesPieces[0];
                secondC = coordinatesPieces[1];
                String val = firstC + "," + secondC;
                coordinates.put(title,val);
            }
        }
    });

    private String getResponseText(String stringUrl) throws IOException {
        StringBuilder response  = new StringBuilder();

        URL url = new URL(stringUrl);
        HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
        if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
        {
            BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
            String strLine = null;
            while ((strLine = input.readLine()) != null)
            {
                response.append(strLine);
            }
            input.close();
        }
        return response.toString();
    }
}
